DROP DATABASE IF EXISTS instrumenti_shop;
CREATE DATABASE instrumenti_shop DEFAULT CHARACTER SET utf8;

USE instrumenti_shop;

GRANT ALL ON instrumenti_shop.* TO 'root'@'%' IDENTIFIED BY 'root';

FLUSH PRIVILEGES;