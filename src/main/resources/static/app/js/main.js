var test = angular.module("testApp", ['ngRoute']);

test.config(['$routeProvider', function($routeProvider) {
	$routeProvider
		.when('/', {
			templateUrl : '/app/partial/home.html'
		})
		.when('/instrumenti', {
			templateUrl : '/app/partial/instrument.html'
		})
		.when('/kategorije', {
			templateUrl : '/app/partial/add_kategorija.html'
		})
		.when('/instrumenti/edit/:id', {
			templateUrl : '/app/partial/edit_instrument.html'
		})
		.otherwise({
			redirectTo: '/'
		});
}]);

test.controller("instrumentCtrl", function($scope, $http, $location){
	
	var baseUrl = "/api/instrumenti";
	var baseUrlKategorija = "/api/kategorije";
	
	/*
	 * za promenu kod naizmeničnog dodavanja i editovanja na istoj stranici
	 */
	$scope.change=false;
	$scope.idEdit=0;
	
	$scope.instrument = [];
	$scope.kategorija = [];
	
	$scope.newInstrument = {};
	$scope.newInstrument.naziv = "";
	$scope.newInstrument.cena = "";
	$scope.newInstrument.kolicina = "";
	$scope.newInstrument.kategorijaId = "";
	$scope.newInstrument.kategorijaNaziv = "";
	
	$scope.newKategorija = {};
	$scope.newKategorija.naziv = "";
	
	$scope.searchedInstrument = {};
	$scope.searchedInstrument.naziv = "";
	$scope.searchedInstrument.cena = "";
	$scope.searchedInstrument.kolicina = "";
	
	$scope.pageNum = 0;
	$scope.totalPages= 1;
	
	// količina kupljenih stavki
	$scope.kolicina = "";
	
var getInstrument = function(){
		
		var conf = {params: {}};
		
		if($scope.searchedInstrument.naziv!=""){
			conf.params.naziv=$scope.searchedInstrument.naziv;
		}
		if($scope.searchedInstrument.cena!=""){
			conf.params.cena=$scope.searchedInstrument.cena;
		}
		if($scope.searchedInstrument.kolicina!=""){
			conf.params.kolicina=$scope.searchedInstrument.kolicina;
		}
		
		conf.params.pageNum = $scope.pageNum;
		
		var promise = $http.get(baseUrl, conf);
		promise.then(
			function success(answ){
				$scope.instrument = answ.data;
				$scope.totalPages=answ.headers("totalPages");
			},
			function error(answ){
				alert("Something went wrong getting all instrumenti data!");
			}
		);
	}

var getKategorija = function(){
	
	var promise = $http.get(baseUrlKategorija);
	
	promise.then(
		function success(answ){
			$scope.kategorija = answ.data;
		},
		function error(answ){
			alert("Something went wrong getting all kategorije data!");
		}
	);
}

getInstrument();
getKategorija();

$scope.addInstrument = function(){
	
	var promise = $http.post(baseUrl, $scope.newInstrument);
	promise.then(
		function success(answ){
			$scope.newInstrument.naziv = "";
			$scope.newInstrument.cena = "";
			$scope.newInstrument.kolicina = "";
			$scope.newInstrument.kategorijaId = "";
			$scope.newInstrument.kategorijaNaziv = "";
			getInstrument();
		},
		function error(answ){
			alert("Something went wrong adding kategorija!");
		}
	);
}

$scope.addKategorija = function(){
	
	var promise = $http.post(baseUrlKategorija, $scope.newKategorija);
	promise.then(
		function success(answ){
			$location.path("/instrumenti");
		},
		function error(answ){
			alert("Something went wrong adding kategorija!");
		}
	);
}

$scope.search = function(){
	$scope.pageNum=0;
	getInstrument();
}

$scope.clearSearch = function(){
	$scope.pageNum=0;
	$scope.searchedInstrument.naziv = "";
	$scope.searchedInstrument.cena = "";
	$scope.searchedInstrument.kolicina = "";
	getInstrument();
}

$scope.changePage = function(direction){
	$scope.pageNum = $scope.pageNum + direction;
	getInstrument();
}

$scope.deleteInstrument = function (id){
	
	$http.delete(baseUrl + "/" + id).then(
		function success(answ){
			getInstrument();
		},
		function error(answ){
			alert("Couldn't delete item!");
		}	
	);
}

// get zbog edita na istoj stranici u formi za dodavanje
$scope.getInstrumentbyId = function(id){
	
	$http.get(baseUrl + "/" + id).then(
		function success(answ){
			$scope.newInstrument = answ.data;
			$scope.change=true;
			$scope.idEdit=id;
		},
		function error(answ){
			alert('Something went wrong geting instrument!');
		}
	);		
}

// za edit na istoj stranici
$scope.edit = function(id){
	
	$http.put(baseUrl + "/" + id, $scope.newInstrument).then(
		function success(answ){
			$scope.newInstrument.naziv = "";
			$scope.newInstrument.cena = "";
			$scope.newInstrument.kolicina = "";
			$scope.newInstrument.kategorijaId = "";
			$scope.newInstrument.kategorijaNaziv = "";
			getInstrument();
		},
		function error(answ){
			alert('Something went wrong editing!');
		}
	);
}

/*
 * za promenu kod naizmeničnog dodavanja i editovanja na istoj stranici
 */

$scope.action = function(){
	if($scope.change==false){
		alert("adding");
		$scope.addInstrument();
	}else{
		alert("editing");
		$scope.change=false;
		$scope.edit($scope.idEdit);
	}
}

// za edit na posebnoj stranici
$scope.goToEdit = function(id){
	$location.path('/instrumenti/edit/' + id);
}

//za odlazak na dodavanje kategorije u novoj stranici
$scope.goToAddKategorija = function(){
	$location.path('/kategorije');
}

$scope.kupi = function(id,kolicina){
	
	var promise = $http.post(baseUrl + "/" + id + "/kupovina/" + kolicina);
	promise.then(
		function success(answ){
			alert("Kupovina je uspešno obavljena!");
			getInstrument();
		},
		function error(answ){
			alert("Kupovina nije uspela!");
		}
	);
}


});

// kontroler za edit u posebnoj stranici
test.controller("editInstrumentCtrl", function($scope, $routeParams, $http, $location){
	
	var baseUrl = "/api/instrumenti";
	var baseUrlKategorija = "/api/kategorije";
	var id = $routeParams.id;
	
	$scope.instrument = [];
	$scope.kategorija = [];
	
	$scope.oldInstrument = {};
	$scope.oldInstrument.naziv = "";
	$scope.oldInstrument.cena = "";
	$scope.oldInstrument.kolicina = "";
	$scope.oldInstrument.kategorijaId = "";
	$scope.oldInstrument.kategorijaNaziv = "";
	
$scope.getInstrumentbyId = function(){
		
		$http.get(baseUrl + "/" + id).then(
			function success(answ){
				$scope.oldInstrument = answ.data;
			},
			function error(answ){
				alert('Something went wrong getting item!');
			}
		);		
	}

var getKategorija = function(){
	
	var promise = $http.get(baseUrlKategorija);
	
	promise.then(
		function success(answ){
			$scope.kategorija = answ.data;
		},
		function error(answ){
			alert("Something went wrong getting all kategorija data!");
		}
	);
}

getKategorija();
$scope.getInstrumentbyId();

$scope.edit = function(){
	
	$http.put(baseUrl + "/" + id, $scope.oldInstrument).then(
		function success(answ){
			$location.path("/instrumenti");
		},
		function error(answ){
			alert('Something went wrong editing!');
		}
	);
}
	
});
