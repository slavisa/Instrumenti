package jwd.test.service;

import java.util.List;

import jwd.test.model.Kategorija;

public interface KategorijaService {
	List<Kategorija> findAll();
	Kategorija findOne(Long id);
	void save(Kategorija kategorija);
}
