package jwd.test.service;

import jwd.test.model.Kupovina;

public interface KupovinaService {
	Kupovina kupi(Long id, Integer kolicina);
}
