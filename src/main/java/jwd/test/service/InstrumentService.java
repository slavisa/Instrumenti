package jwd.test.service;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;

import jwd.test.model.Instrument;

public interface InstrumentService {
	Page<Instrument> findAll(int pageNum);
	Instrument findOne(Long id);
	void save(Instrument instrument);
	void remove(Long id);
	Page<Instrument> findByKategorijaId(int pageNum, Long instrumentId);
	Page<Instrument> pretraga(
			@Param("naziv") String naziv,
			@Param("cena") Float cena, 
			@Param("kolicina") Integer kolicina, 
			int pageNum
			);
}
