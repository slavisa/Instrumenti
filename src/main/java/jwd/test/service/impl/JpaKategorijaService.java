package jwd.test.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jwd.test.model.Kategorija;
import jwd.test.repository.KategorijaRepository;
import jwd.test.service.KategorijaService;
@Service
public class JpaKategorijaService implements KategorijaService {

	@Autowired
	KategorijaRepository kategorijaRepository;
	
	@Override
	public List<Kategorija> findAll() {
		return kategorijaRepository.findAll();
	}

	@Override
	public Kategorija findOne(Long id) {
		return kategorijaRepository.findOne(id);
	}

	@Override
	public void save(Kategorija kategorija) {
		kategorijaRepository.save(kategorija);
	}

}
