package jwd.test.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import jwd.test.model.Instrument;
import jwd.test.repository.InstrumentRepository;
import jwd.test.service.InstrumentService;
@Service
public class JpaInstrumentService implements InstrumentService {

	@Autowired
	InstrumentRepository instrumentRepository;
	
	@Override
	public Page<Instrument> findAll(int pageNum) {
		return instrumentRepository.findAll(new PageRequest(pageNum, 5));
	}

	@Override
	public Instrument findOne(Long id) {
		return instrumentRepository.findOne(id);
	}

	@Override
	public void save(Instrument instrument) {
		instrumentRepository.save(instrument);
	}

	@Override
	public void remove(Long id) {
		instrumentRepository.delete(id);
	}

	@Override
	public Page<Instrument> findByKategorijaId(int pageNum, Long instrumentId) {
		return instrumentRepository.findByKategorijaId(instrumentId, new PageRequest(pageNum, 5));
	}

	@Override
	public Page<Instrument> pretraga(String naziv, Float cena, Integer kolicina, int pageNum) {
		if (naziv != null) {
			naziv = "%" + naziv + "%";
		}
		
		return instrumentRepository.pretraga(naziv, cena, kolicina, new PageRequest(pageNum, 5));
	}

}
