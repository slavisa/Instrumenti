package jwd.test.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jwd.test.model.Kupovina;
import jwd.test.model.Instrument;
import jwd.test.repository.KupovinaRepository;
import jwd.test.service.KupovinaService;
import jwd.test.service.InstrumentService;
@Service
public class JpaKupovinaService implements KupovinaService {
	
	@Autowired
	KupovinaRepository kupovinaRepository;
	
	@Autowired
	InstrumentService instrumentService;
	
	@Override
	public Kupovina kupi(Long id, Integer kolicina) {
		
		if(id==null) {
			throw new IllegalArgumentException("Id number can not be null!");
		}
		
		Instrument instrument = instrumentService.findOne(id);
		
		if(instrument==null) {
			throw new IllegalArgumentException("Non-existing instrument to buy!");
		}
		
		Kupovina kupovina = null;
		
		if(instrument.getKolicina()>=kolicina) {
			kupovina = new Kupovina();
			instrument.setKolicina(instrument.getKolicina()-kolicina);
			kupovina.setInstrument(instrument);
			instrument.addKupovina(kupovina);
			instrumentService.save(instrument);
			kupovinaRepository.save(kupovina);
		}
		
		return kupovina;
	}

}
