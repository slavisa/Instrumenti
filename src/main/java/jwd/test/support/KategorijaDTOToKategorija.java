package jwd.test.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import jwd.test.model.Kategorija;
import jwd.test.service.KategorijaService;
import jwd.test.web.dto.KategorijaDTO;
@Component
public class KategorijaDTOToKategorija implements Converter<KategorijaDTO, Kategorija> {
	
	@Autowired
	KategorijaService kategorijaService;
	
	@Override
	public Kategorija convert(KategorijaDTO dto) {
		Kategorija kategorija;
		
		if(dto.getId()==null) {
			kategorija = new Kategorija();
		}else {
			kategorija = kategorijaService.findOne(dto.getId());
		}
		
		kategorija.setNaziv(dto.getNaziv());
		
		return kategorija;
	}

	public List<Kategorija> convert(List<KategorijaDTO> dtos){
		List<Kategorija> kategorije = new ArrayList<>();
		
		for (KategorijaDTO dto : dtos) {
			kategorije.add(convert(dto));
		}
		
		return kategorije;
	}
	
}
