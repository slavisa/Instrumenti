package jwd.test.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import jwd.test.model.Instrument;
import jwd.test.web.dto.InstrumentDTO;
@Component
public class InstrumentToInstrumentDTO implements Converter<Instrument, InstrumentDTO> {
	
	@Override
	public InstrumentDTO convert(Instrument instrument) {
		InstrumentDTO dto = new InstrumentDTO();
		
		dto.setId(instrument.getId());
		dto.setNaziv(instrument.getNaziv());
		dto.setKategorijaId(instrument.getKategorija().getId());
		dto.setKategorijaNaziv(instrument.getKategorija().getNaziv());
		dto.setKolicina(instrument.getKolicina());
		dto.setCena(instrument.getCena());
		
		return dto;
	}
	
	public List<InstrumentDTO> convert(List<Instrument> instrument){
		List<InstrumentDTO> dtos = new ArrayList<>();
		
		for (Instrument i : instrument) {
			dtos.add(convert(i));
		}
		
		return dtos;
	}
}
