package jwd.test.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import jwd.test.model.Kategorija;
import jwd.test.web.dto.KategorijaDTO;
@Component
public class KategorijaToKategorijaDTO implements Converter<Kategorija, KategorijaDTO> {

	@Override
	public KategorijaDTO convert(Kategorija kategorija) {
		KategorijaDTO dto = new KategorijaDTO();
		
		dto.setId(kategorija.getId());
		dto.setNaziv(kategorija.getNaziv());
		
		return dto;
	}
	
	public List<KategorijaDTO> convert(List<Kategorija> kategorija){
		List<KategorijaDTO> dtos = new ArrayList<>();
		
		for (Kategorija k : kategorija) {
			dtos.add(convert(k));
		}
		
		return dtos;
	}

}
