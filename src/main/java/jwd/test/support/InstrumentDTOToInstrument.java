package jwd.test.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import jwd.test.model.Instrument;
import jwd.test.service.KategorijaService;
import jwd.test.service.InstrumentService;
import jwd.test.web.dto.InstrumentDTO;
@Component
public class InstrumentDTOToInstrument implements Converter<InstrumentDTO, Instrument> {
	
	@Autowired
	InstrumentService instrumentService;
	
	@Autowired
	KategorijaService kategorijaService;
	
	@Override
	public Instrument convert(InstrumentDTO dto) {
		Instrument instrument;
		
		if(dto.getId()==null) {
			instrument = new Instrument();
		}else {
			instrument = instrumentService.findOne(dto.getId());
		}
		
		instrument.setNaziv(dto.getNaziv());
		instrument.setKategorija(kategorijaService.findOne(dto.getKategorijaId()));
		instrument.setCena(dto.getCena());
		instrument.setKolicina(dto.getKolicina());
		
		return instrument;
	}

	public List<Instrument> convert(List<InstrumentDTO> dtos){
		List<Instrument> instrumenti = new ArrayList<>();
		
		for (InstrumentDTO dto : dtos) {
			instrumenti.add(convert(dto));
		}
		
		return instrumenti;
	}
}
