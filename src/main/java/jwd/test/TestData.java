package jwd.test;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import jwd.test.model.Kategorija;
import jwd.test.model.Instrument;
import jwd.test.service.KategorijaService;
import jwd.test.service.InstrumentService;

@Component
public class TestData {
	@Autowired
	InstrumentService instrumentService;
	@Autowired
	KategorijaService kategorijaService;

	@PostConstruct
	public void init() {

		Kategorija k = new Kategorija();
		k.setNaziv("Gitare");
		kategorijaService.save(k);

		Instrument instrument = new Instrument();
		instrument.setNaziv("Fender");
		instrument.setCena((float) (15000));
		instrument.setKolicina(15);
		instrument.setKategorija(k);
		k.addInstrument(instrument);
		instrumentService.save(instrument);

		instrument = new Instrument();
		instrument.setNaziv("Yamaha");
		instrument.setCena((float) (14000));
		instrument.setKolicina(20);
		instrument.setKategorija(k);
		k.addInstrument(instrument);
		instrumentService.save(instrument);

		instrument = new Instrument();
		instrument.setNaziv("Gibson");
		instrument.setCena((float) (20000));
		instrument.setKolicina(10);
		instrument.setKategorija(k);
		k.addInstrument(instrument);
		instrumentService.save(instrument);

		k = new Kategorija();
		k.setNaziv("Klaviri");
		kategorijaService.save(k);

		instrument = new Instrument();
		instrument.setNaziv("Petrof");
		instrument.setCena((float) (85000));
		instrument.setKolicina(2);
		instrument.setKategorija(k);
		k.addInstrument(instrument);
		instrumentService.save(instrument);

		k = new Kategorija();
		k.setNaziv("Bubnjevi");
		kategorijaService.save(k);

		instrument = new Instrument();
		instrument.setNaziv("Kompet bubnjeva za rock");
		instrument.setCena((float) (25000));
		instrument.setKolicina(3);
		instrument.setKategorija(k);
		k.addInstrument(instrument);
		instrumentService.save(instrument);

		instrument = new Instrument();
		instrument.setNaziv("Kompet bubnjeva za jazz");
		instrument.setCena((float) (35000));
		instrument.setKolicina(2);
		instrument.setKategorija(k);
		k.addInstrument(instrument);
		instrumentService.save(instrument);

		k = new Kategorija();
		k.setNaziv("Harmonike");
		kategorijaService.save(k);

		instrument = new Instrument();
		instrument.setNaziv("Klasična harmonika");
		instrument.setCena((float) (60000));
		instrument.setKolicina(3);
		instrument.setKategorija(k);
		k.addInstrument(instrument);
		instrumentService.save(instrument);

		instrument = new Instrument();
		instrument.setNaziv("Folk harmonika");
		instrument.setCena((float) (35000));
		instrument.setKolicina(5);
		instrument.setKategorija(k);
		k.addInstrument(instrument);
		instrumentService.save(instrument);

	}

}
