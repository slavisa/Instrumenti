package jwd.test.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import jwd.test.model.Kategorija;
import jwd.test.service.KategorijaService;
import jwd.test.support.KategorijaDTOToKategorija;
import jwd.test.support.KategorijaToKategorijaDTO;
import jwd.test.web.dto.KategorijaDTO;

@RestController
@RequestMapping("/api/kategorije")
public class ApiKategorijaController {
	
	@Autowired
	private KategorijaService kategorijaService;
	@Autowired
	private KategorijaToKategorijaDTO toDTO;
	@Autowired
	private KategorijaDTOToKategorija toKategorija;
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<KategorijaDTO>> get(){
		
		List<Kategorija> kategorije = kategorijaService.findAll();
		
		return new ResponseEntity<>(toDTO.convert(kategorije),HttpStatus.OK);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public ResponseEntity<KategorijaDTO> get(
			@PathVariable Long id){
		
		Kategorija kategorija = kategorijaService.findOne(id);
		
		if(kategorija==null) {
			return  new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<>(toDTO.convert(kategorija),HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<KategorijaDTO> add(@RequestBody @Validated KategorijaDTO noviKategorija) {

		Kategorija kategorija = toKategorija.convert(noviKategorija);

		kategorijaService.save(kategorija);

		return new ResponseEntity<>(toDTO.convert(kategorija), HttpStatus.CREATED);

	}
	
	@ExceptionHandler
	public ResponseEntity<Void> validationHandler(DataIntegrityViolationException e) {

		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}
}
