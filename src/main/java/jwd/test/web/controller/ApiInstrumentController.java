package jwd.test.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import jwd.test.model.Kupovina;
import jwd.test.model.Instrument;
import jwd.test.service.KupovinaService;
import jwd.test.service.InstrumentService;
import jwd.test.support.KupovinaToKupovinaDTO;
import jwd.test.support.InstrumentDTOToInstrument;
import jwd.test.support.InstrumentToInstrumentDTO;
import jwd.test.web.dto.KupovinaDTO;
import jwd.test.web.dto.InstrumentDTO;

@RestController
@RequestMapping("/api/instrumenti")
public class ApiInstrumentController {

	@Autowired
	private InstrumentService instrumentService;
	@Autowired
	private InstrumentToInstrumentDTO toDTO;
	@Autowired
	private InstrumentDTOToInstrument toInstrument;
	@Autowired
	private KupovinaService kupovinaService;
	@Autowired
	private KupovinaToKupovinaDTO toKupovinaDTO;

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<InstrumentDTO>> get(@RequestParam(required = false) String naziv,
			@RequestParam(required = false) Float cena, @RequestParam(required = false) Integer kolicina,
			@RequestParam(defaultValue = "0") int pageNum) {

		Page<Instrument> instrumenti;

		if (naziv != null || cena != null || kolicina != null) {
			instrumenti = instrumentService.pretraga(naziv, cena, kolicina, pageNum);
		} else {
			instrumenti = instrumentService.findAll(pageNum);
		}

		HttpHeaders headers = new HttpHeaders();
		headers.add("totalPages", Integer.toString(instrumenti.getTotalPages()));

		return new ResponseEntity<>(toDTO.convert(instrumenti.getContent()), headers, HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<InstrumentDTO> get(@PathVariable Long id) {

		Instrument instrument = instrumentService.findOne(id);

		if (instrument == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(toDTO.convert(instrument), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<InstrumentDTO> add(@RequestBody @Validated InstrumentDTO noviInstrument) {

		Instrument instrument = toInstrument.convert(noviInstrument);

		instrumentService.save(instrument);

		return new ResponseEntity<>(toDTO.convert(instrument), HttpStatus.CREATED);

	}

	@RequestMapping(method = RequestMethod.POST, value = "/{id}/kupovina/{kolicina}")
	public ResponseEntity<KupovinaDTO> buy(@PathVariable Long id, @PathVariable Integer kolicina) {

		Kupovina kupovina = kupovinaService.kupi(id, kolicina);

		if (kupovina == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(toKupovinaDTO.convert(kupovina), HttpStatus.CREATED);
		}

	}

	@RequestMapping(method = RequestMethod.PUT, value = "/{id}", consumes = "application/json")
	public ResponseEntity<InstrumentDTO> edit(@PathVariable Long id,
			@RequestBody @Validated InstrumentDTO noviInstrument) {

		if (!id.equals(noviInstrument.getId())) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		Instrument instrument = toInstrument.convert(noviInstrument);

		instrumentService.save(instrument);

		return new ResponseEntity<>(toDTO.convert(instrument), HttpStatus.CREATED);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
	public ResponseEntity<InstrumentDTO> delete(@PathVariable Long id) {

		instrumentService.remove(id);

		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@ExceptionHandler
	public ResponseEntity<Void> validationHandler(DataIntegrityViolationException e) {

		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}

}
