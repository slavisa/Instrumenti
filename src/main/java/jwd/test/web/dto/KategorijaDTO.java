package jwd.test.web.dto;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

public class KategorijaDTO {
	
	private Long id;
	@NotBlank(message="Naziv must not be empty")
	@Size(max=40)
	private String naziv;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	
}
