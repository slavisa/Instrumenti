package jwd.test.web.dto;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

public class InstrumentDTO {
	private Long id;
	@NotBlank(message="Naziv must not be empty")
	@Size(max=40)
	private String naziv;
	@Min(100)
	@Max(150000)
	private Float cena;
	@Min(0)
	@Max(50)
	private Integer kolicina;
	private Long kategorijaId;
	private String kategorijaNaziv;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public Long getKategorijaId() {
		return kategorijaId;
	}
	public void setKategorijaId(Long kategorijaId) {
		this.kategorijaId = kategorijaId;
	}
	public String getKategorijaNaziv() {
		return kategorijaNaziv;
	}
	public void setKategorijaNaziv(String kategorijaNaziv) {
		this.kategorijaNaziv = kategorijaNaziv;
	}
	public Float getCena() {
		return cena;
	}
	public void setCena(Float cena) {
		this.cena = cena;
	}
	public Integer getKolicina() {
		return kolicina;
	}
	public void setKolicina(Integer kolicina) {
		this.kolicina = kolicina;
	}
	
}
