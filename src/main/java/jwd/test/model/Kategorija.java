package jwd.test.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table
public class Kategorija {
	@Id
	@GeneratedValue
	@Column
	private Long id;
	@Column(nullable=false, unique=true)
	private String naziv;
	@OneToMany(mappedBy="kategorija",cascade=CascadeType.ALL,fetch=FetchType.LAZY)
	private List<Instrument> instrumenti = new ArrayList<>();
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public List<Instrument> getInstrumenti() {
		return instrumenti;
	}
	public void setInstrumenti(List<Instrument> instrumenti) {
		this.instrumenti = instrumenti;
	}
	
	public void addInstrument(Instrument instrument) {
		this.instrumenti.add(instrument);
		
		if(!this.equals(instrument.getKategorija())) {
			instrument.setKategorija(this);
		}
	}
}
