package jwd.test.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table
public class Instrument {
	@Id
	@GeneratedValue
	@Column
	private Long id;
	@Column(nullable=false, unique=true)
	private String naziv;
	@Column(nullable=false)
	private Integer kolicina;
	@Column(nullable=false)
	private Float cena;
	@ManyToOne(fetch = FetchType.EAGER)
	private Kategorija kategorija;
	@OneToMany(mappedBy = "instrument", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<Kupovina> kupovine = new ArrayList<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public Kategorija getKategorija() {
		return kategorija;
	}

	public void setKategorija(Kategorija kategorija) {
		this.kategorija = kategorija;

		if (kategorija != null && kategorija.getInstrumenti().contains(this)) {
			kategorija.getInstrumenti().add(this);
		}
	}

	public Integer getKolicina() {
		return kolicina;
	}

	public void setKolicina(Integer kolicina) {
		this.kolicina = kolicina;
	}

	public Float getCena() {
		return cena;
	}

	public void setCena(Float cena) {
		this.cena = cena;
	}

	public List<Kupovina> getKupovine() {
		return kupovine;
	}

	public void setKupovine(List<Kupovina> kupovine) {
		this.kupovine = kupovine;
	}

	public void addKupovina(Kupovina kupovina) {
		this.kupovine.add(kupovina);

		if (!this.equals(kupovina.getInstrument())) {
			kupovina.setInstrument(this);
		}
	}
}
