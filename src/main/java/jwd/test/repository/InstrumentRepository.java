package jwd.test.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import jwd.test.model.Instrument;
@Repository
public interface InstrumentRepository extends JpaRepository<Instrument, Long> {

	Page<Instrument> findByKategorijaId(Long instrumentId, Pageable pageRequest);
	@Query("SELECT i FROM Instrument i WHERE "
			+ "(:naziv IS NULL or i.naziv like :naziv ) AND "
			+ "(:cena IS NULL or i.cena < :cena ) AND "
			+ "(:kolicina IS NULL or i.kolicina >= :kolicina )"
			)
	Page<Instrument> pretraga(
			@Param("naziv") String naziv, 
			@Param("cena") Float cena, 
			@Param("kolicina") Integer kolicina, 
			Pageable pageRequest);

}
