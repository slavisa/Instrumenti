package jwd.test.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import jwd.test.model.Kategorija;
@Repository
public interface KategorijaRepository extends JpaRepository<Kategorija, Long> {

}
